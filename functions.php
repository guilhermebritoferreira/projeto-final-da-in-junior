<?php

/* para poder usar os templates do woocomerce*/

function add_comestema(){
    add_theme_support('woocommerce', array(
        'thumbnail_image_width' => 240,
        'single_image_width' => 500,

        'product_grid' => array(
            'default_rows' => 4,
            'min_rows' => 4,
            'max_rows' => 8,
            'default_columns' => 4,
            'min_columns' => 4,
            'max_columns' => 5,
        ),
    ));
};


add_action('after_setup_theme','add_comestema');


add_filter( 'woocommerce_product_related_posts', 'woocommerce_get_direct_related_products' );

function woocommerce_get_direct_related_products() {
    global $woocommerce, $product;

    // Related products are found from category
    $cats_array = array(0);

    // Get categories
    $terms = wp_get_post_terms( $product->id, 'product_cat' );

    //Select only the category which doesn't have any children
    foreach ( $terms as $term ) {
        $children = get_term_children( $term->term_id, 'product_cat' );
        if ( !sizeof( $children ) )
            $cats_array[] = $term->term_id;
    }

    // Don't bother if none are set
    if ( sizeof( $cats_array ) == 1 ) return array();

    // Meta query
    $meta_query = array();
    $meta_query[] = $woocommerce->query->visibility_meta_query();
    $meta_query[] = $woocommerce->query->stock_status_meta_query();

    $limit = 5;

    // Get the posts
    return array(
        'orderby'       => 'rand',
        'posts_per_page'=> $limit,
        'post_type'     => 'product',
        'fields'        => 'ids',
        'meta_query'    => $meta_query,
        'tax_query'     => array(
            array(
                'taxonomy'  => 'product_cat',
                'field'     => 'id',
                'terms'     => $cats_array
            )
        )
    );
}


function url(){
    return sprintf(
      "%s://%s%s",
      isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
      $_SERVER['SERVER_NAME'],
      $_SERVER['REQUEST_URI']
    );
  }
  
  

  
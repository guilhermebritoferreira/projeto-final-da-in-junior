<?php 
//Template Name: Shop
?> 

<?php
        /* wp_nav_menu([
            'menu'=> 'categorias',
            'container'=> 'nav',
            'container_class'=>'categorias',
        ]); */
        ?>

<?php get_header(); ?>
    <main>
    <section class="marginpainel">
        <h1 class='tituloSection'> SELECIONE UMA CATEGORIA </h1>
        <div class="categoriashome">
                        <?php 
                            $taxonomy     = 'product_cat';
                            $orderby      = 'name';  
                            $show_count   = 0;      // 1 for yes, 0 for no
                            $pad_counts   = 0;      // 1 for yes, 0 for no
                            $hierarchical = 1;      // 1 for yes, 0 for no  
                            $title        = '';  
                            $empty        = 1;
                        
                            $args = array(
                                'taxonomy'     => $taxonomy,
                                'orderby'      => $orderby,
                                'show_count'   => $show_count,
                                'pad_counts'   => $pad_counts,
                                'hierarchical' => $hierarchical,
                                'title_li'     => $title,
                                'hide_empty'   => $empty
                            );


                            $categories = get_categories($args);
                            if($categories){
                                foreach($categories as $category){
                                    $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
                                    $image = wp_get_attachment_url($thumbnail_id);
                                    $link = get_term_link($category->slug, 'product_cat');
                                    echo "<a href='$link'>
                                    <div class = 'categoriasEFotos' id='notYetShown'>
                                    <img src='{$image}' alt=''></img>
                                    <p>{$category->name}</p>
                                    </div></a>";
                                }
                            } 
                        ?>
                        
                    </div>
    
                </ul>
                
                


    </section>
    </main>
    <?php get_footer(); ?>
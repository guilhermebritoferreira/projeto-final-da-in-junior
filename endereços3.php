<?php 
//Template Name: Endereços 3 
?> 

<?php get_header(); ?>
    <main>
        <nav class='navbar'>
                <ul class= 'navbuttons'>
                    <li><a href="http://comes-e-bebes-in-junior.local/painel/">PAINEL</a></li>
                    <li>PEDIDOS</li>
                    <li>ENDEREÇOS</li>
                    <li>SAIR</li>
                </ul>
                </nav>
        <section class="marginpainel">
            <p class='introPainel'>Os endereços a seguir serão usados na página de finalizar pedido como endereços padrões, mas é possível modificá-los durante a finalização do pedido</p>
            <h1 class='formTitle'>Edite seu endereço</h1>
            <ul>
                <div class='nomesobrenome'>
                    <div class='flexform'>
                        <li class=titleInput>Nome</li>
                        <li><input class="inputForm" id="inputNomeAdress2" type="text" placeholder="Digite seu nome" name="Nome"></li>
                    </div>
                    <div class='flexform'>
                        <li class=titleInput>Sobrenome</li>
                        <li><input class="inputForm" id="inputSobrenomeAdress2" type="text" placeholder="Digite seu sobrenome" name="Sobrenome"></li>
                    </div>
                </div>
                <li class=titleInput>CEP</li>
                <li><input class="inputForm" id="inputCEPAdress2" type="text" placeholder="Digite seu CEP" name="CEP"></li>
                <li class=titleInput>Logradouro</li>    
                <li><input class="inputForm" id="inputLogradouroAdress2" type="text" placeholder="Digite seu Logradouro" name="Logradouro"></li>
                <li class=titleInput>Complemento</li>
                <li><input class="inputForm" id="inputComplementoAdress2" type="text" placeholder="Digite seu complemento" name="Complemento"></li>
                <div class='nomesobrenome'>
                    <div class="flexform">
                        <li class=titleInput>Bairro</li>
                        <li><input class="inputForm" id="inputBairroAdress2" type="text" placeholder="Digite seu bairro" name="Bairro"></li>
                    </div>
                    <div class="flexform">
                        <li class=titleInput>Cidade</li>
                        <li><input class="inputForm" id="inputCidadeAdress2" type="text" placeholder="Digite sua cidade" name="Cidade"></li>
                    </div>
                </div>
            </ul>
            <div id="alinhador">
                <button id='salvarAlteracoes'>SALVAR ALTERAÇÕES</button>
                </div>
        </section>
    </main>
    <?php get_footer(); ?>
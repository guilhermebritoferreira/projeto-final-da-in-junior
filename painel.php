<?php 
//Template Name: Painel 
?> 
<?php get_header(); ?>
    <main>
        <nav class='navbar'>
            <ul class= 'navbuttons'>
                <li><a href="http://comes-e-bebes-in-junior.local/painel/">PAINEL</a></li>
                <li>PEDIDOS</li>
                <li>ENDEREÇOS</li>
                <li>SAIR</li>
            </ul>
            </nav>
            <section class = marginpainel>
                <p class='introPainel'>Olá, usuário (não é usuário? Sair)<br><br>A partir do painel de controle de sua conta, você pode ver suas compras recentes, gerenciar seus endereços de entrega e faturamento, e editar sua senha e detalhes da conta</p>
            
            <form>
                <ul>
                    <div class="nomesobrenome">
                        <div class="flexform">
                            <li class=titleInput>Nome</li>
                            <li><input class="inputForm"id="inputNome"type="text" placeholder="Digite seu nome" name="Nome"></li>
                        </div>
                        <div class="flexform">
                            <li class=titleInput>Sobrenome</li>
                            <li><input class="inputForm"id="inputSobrenome"type="text" placeholder="Digite seu sobrenome" name="Sobrenome"></li>
                        </div>
                    </div>
                        <li class=titleInput>Email</li>
                        <li><input class="inputForm"id="inputEmail"type="text" placeholder="Digite seu email" name="Email"></li>
                        <li class=titleInput>Senha atual (deixe em branco para não alterar)</li>
                        <li><input class="inputForm"id="inputSenhaAtual"type="text" placeholder="Digite sua senha atual" name="SenhaAtual"></li>
                        <li class=titleInput>Nova senha (deixe em branco para não alterar)</li>
                        <li><input class="inputForm"id="inputNovaSenha"type="text" placeholder="Digite sua nova senha" name="NovaSenha"></li>
                        <li class=titleInput>Confirmar nova senha</li>
                        <li><input class="inputForm"id="inputConfirmar"type="text" placeholder="Confirme sua nova senha" name="ConfirmarNovaSenha"></li>
                </ul>
                <div id="alinhador">
                <button id='salvarAlteracoes'>SALVAR ALTERAÇÕES</button>
                </div>
            </form>
            </section>
    </main>
    <?php get_footer(); ?>
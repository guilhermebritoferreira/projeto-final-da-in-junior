<!DOCTYPE html>
<html lang="pt-br">
    
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comes e Bebes</title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <?php wp_head(); ?>
</head>
<body>
    <header>
        
        <figure><a href ="<?php echo "http://" . $_SERVER['SERVER_NAME'] ; ?>"><img id="logo" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/logo.png"><a></figure>
        <div id="searchBox">
        <form action="/product-category/" method="get">
            <input type="image" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/lupinha.png" id="botaoLupa">
            <input id="searchText"type="text" placeholder="Pesquisar..." name="s" value="<?php echo the_search_query();?>"> 
            <input type="text" name="post_type" value="product" class="hidden">
        </form>
        </div>
        <a href="/home/lista-de-produtos/"> <button id="botaoFacaUmPedido">Faça um pedido</button></a>
        <img id="cartLogo" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/carrinho.png">
        <a href="/my-account/"><img id="avatarLogo" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/avatarsim.png"></a>
    
        <div class="container-carrinho hidden">
        </div>
        <div class="aparece-carrinho hidden">
            <span id="titulo-carrinho">Carrinho</span>
            <span id="fechar">X</span>
            <div id="barrinha">
            <div id="margemBarrinha">
                <div class="greyBlueBoxCart">
            <?php 
                global $woocommerce;
                $items = $woocommerce->cart->get_cart(); 

                echo '<div id="eachProductCart">';
                foreach($items as $item => $values) { 
                    $_product =  wc_get_product( $values['data']->get_id()); 
                    echo '<div>';
                    echo $_product->get_image();
                    echo '<div class="flexCartItem">';
                    echo '<br> Quantidade: '.$values['quantity'].'<br>'; 
                    $price = get_post_meta($values['product_id'] , '_price', true);
                    echo "<b> Preço: ".$price."</b> <br>";
                    echo '</div>';
                    echo '</div>';
                    

                } 
                echo '</div>';
                
            ?>
            </div>
            <input type="number" step="<?php echo esc_attr( $step ); ?>" 
            <?php if ( is_numeric( $min_value ) ) : ?>min="<?php echo esc_attr( $min_value ); ?>
                "<?php endif; ?> 
                <?php if ( is_numeric( $max_value ) ) : ?>
                    max="<?php echo esc_attr( $max_value ); ?>
                    "<?php endif; ?> name="<?php echo esc_attr( $input_name ); ?>
                    " value="<?php echo esc_attr( $input_value ); ?>
                    " title="<?php _ex( 'Qty', 'Product quantity input tooltip', 'woocommerce' ) ?>
                    " class="input-text qty text" size="4" />

                </div></div>
            <p id="mostrar-total">Total do Carrinho: <b><?php echo WC()->cart->get_cart_total(); ?></b></p>
            <div class="centralizar">
            <button><a id="comprar-carrinho" href="<?php echo wc_get_checkout_url(); ?>">COMPRAR</a></button>
                </div>
            </div>

            <script>
            const carrinho = document.querySelector('#cartLogo')
            carrinho.addEventListener('click', (e)=>{
                const apareceCarrinho = document.querySelector('.aparece-carrinho')
                const containerCarrinho = document.querySelector('.container-carrinho')

                apareceCarrinho.classList.toggle("hidden");
                containerCarrinho.classList.toggle("hidden");
            })

            const fecharCarrinho = document.querySelector('#fechar')
            fecharCarrinho.addEventListener('click', (e)=>{
                const apareceCarrinho = document.querySelector('.aparece-carrinho')
                const containerCarrinho = document.querySelector('.container-carrinho')

                apareceCarrinho.classList.toggle("hidden");
                containerCarrinho.classList.toggle("hidden");
            })

        </script>
        
    
    
    </header>

    
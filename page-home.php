<?php 
//Template Name: Home 
?> 
<?php get_header(); ?>
    <main>
        <section id="firstSectionIndex" class=purpleBox>
            <h1 id="titleComesBebes"> Comes&Bebes </h1>
            <p id="subtitleComesBebes"> O restaurante para todas as fomes </p>
        </section>
        <section id="secondSectionIndex" class=greyBlueBox>
            <h2 class="tituloSection" id="conhecaNossaLoja">CONHEÇA NOSSA LOJA</h2>
            <h3 class="h3index"> Tipos de pratos principais</h3>
                
                
            <div class="categoriashome">
                    <?php 
                        $taxonomy     = 'product_cat';
                        $orderby      = 'name';  
                        $show_count   = 0;      // 1 for yes, 0 for no
                        $pad_counts   = 0;      // 1 for yes, 0 for no
                        $hierarchical = 1;      // 1 for yes, 0 for no  
                        $title        = '';  
                        $empty        = 1;
                      
                        $args = array(
                               'taxonomy'     => $taxonomy,
                               'orderby'      => $orderby,
                               'show_count'   => $show_count,
                               'pad_counts'   => $pad_counts,
                               'hierarchical' => $hierarchical,
                               'title_li'     => $title,
                               'hide_empty'   => $empty
                        );

                        $categories = get_categories($args);
                        if($categories){
                            foreach($categories as $category){
                                $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
                                $image = wp_get_attachment_url($thumbnail_id);
                                $link = get_term_link($category->slug, 'product_cat');
                                echo "<a href='$link'>
                                <div class = 'categoriasEFotos'>
                                <img src='{$image}' alt=''></img>
                                <p>{$category->name}</p>
                                </div></a>";
                            }
                        } 
                    ?>
                    
                </div>
            
            
            
            
            
            
                <h3 class="h3index"> Pratos do dia de hoje: </h3>
          
            <div id="diadasemana">
          <?php   
                setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                date_default_timezone_set('America/Sao_Paulo');
                $day=strftime(ucwords("%w",time()));
                switch ($day) {
                    case 0:
                        echo "DOMINGO";
                        break;
                    case 1:
                        echo "SEGUNDA";
                        break;
                    case 2:
                        echo "TERÇA";
                        break;
                    case 3:
                        echo "QUARTA";
                        break;
                    case 4:
                        echo "QUINTA";
                        break;
                    case 5:
                        echo "SEXTA";
                        break;
                    case 6:
                        echo "SÁBADO";
                        break;
                    
                }
                ?>
                </div>
                <div class="categoriashome">
                <?php 
                        
                    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                    date_default_timezone_set('America/Sao_Paulo');
                    $day=strftime(ucwords("%a",time()));

                    $args = array(
                        'post_type'      => 'product',
                        'posts_per_page' => 4,
                        'product_tag'       => $day,
                    );
                
                    $loop = new WP_Query( $args );
                
                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        $imagem = woocommerce_get_product_thumbnail();
                        $titulo = get_the_title();
                        $preco = wc_price($product->get_price(1,$product->get_price()));;  
                        $link = get_permalink();
                        echo "<a href='$link'>
                        <div class = 'diadasemanaEFotos'>
                        <img {$imagem}</img>
                        <div id = 'captiondiadasemana'>
                        <p  >{$titulo}</p>
                        <div id='precoECart'>
                        <p>{$preco}</p> 
                        <button class = 'addToCartBut'><img id='cartbutton'src='".get_stylesheet_directory_uri()."/imgs/cart.png'></img></button>
                        </div>
                        </div>
                        </div>
                        </a>";
                        
                    endwhile;
                
                    wp_reset_query();
                ?>
                    </div>
            

            <div class="centralizar">
                <a href="http://comes-e-bebes-in-junior.local/home/lista-de-produtos/"><button id=botaoOutrasOpcoes>Veja outras opções</button></a>
            </div>
        </section>
        <section id="thirdSectionIndex" class=purpleBox>
            <h2 id="visiteNossaLoja">VISITE NOSSA LOJA FÍSICA</h2>
            <div id="visitFoot">
                <div id="mapRefs">
                    <div id="googlemaps">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.230770329501!2d-43.133855385468344!3d-22.90485754355959!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817e444e692b%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1632795840194!5m2!1spt-BR!2sbr"
                        width="300" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
                        <div class="mapSubs">
                            <img id="iconmapref" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/talher.png">
                            <sub id="submapref">Rua lorem ipsum, 123, LI, Brasil</sub>
                        </div>
                        <div class="mapSubs">
                            <img id="iconmapref" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/telefone.png">
                            <sub  id="submapref">(XX) XXXX-XXXX</sub>
                        </div>
                </div>


                <ul class="slider">
    <li>
          <input type="radio" id="slide1" name="slide" checked>
          <label for="slide1"></label>
          <img id="pessoalcomendo" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/pessoalcomendo.png">
    </li>
    <li>
          <input type="radio" id="slide2" name="slide">
          <label for="slide2"></label>
          <img id="pessoalcomendo" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/pessoalcomendo2.webp">
    </li>
    <li>
          <input type="radio" id="slide3" name="slide">
          <label for="slide3"></label>
          <img id="pessoalcomendo" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/pessoalcomendo3.webp">
    </li>
</ul>
            
            </div>
        </section>
    </main>
    <?php get_footer(); ?>

    


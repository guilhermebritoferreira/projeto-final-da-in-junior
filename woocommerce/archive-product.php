<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */

?>
<section class="marginpainel">
<h1 class='tituloSection'> SELECIONE UMA CATEGORIA </h1>
        <div class="categoriashome">
                        <?php 
                            $taxonomy     = 'product_cat';
                            $orderby      = 'name';  
                            $show_count   = 0;      // 1 for yes, 0 for no
                            $pad_counts   = 0;      // 1 for yes, 0 for no
                            $hierarchical = 1;      // 1 for yes, 0 for no  
                            $title        = '';  
                            $empty        = 1;
                        
                            $args = array(
                                'taxonomy'     => $taxonomy,
                                'orderby'      => $orderby,
                                'show_count'   => $show_count,
                                'pad_counts'   => $pad_counts,
                                'hierarchical' => $hierarchical,
                                'title_li'     => $title,
                                'hide_empty'   => $empty
                            );


                            $categories = get_categories($args);
                            if($categories){
                                foreach($categories as $category){
                                    $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
                                    $image = wp_get_attachment_url($thumbnail_id);
									$link = get_term_link($category->slug, 'product_cat');
                                    echo "<a href='$link'>
									<div class = 'categoriasEFotos'>
                                    <img src='{$image}' alt=''></img>
                                    <p>{$category->name}</p>
                                    </div></a>";
                                }
                            } 
                        ?>
                        
                    </div>
    
        <h1 class='tituloSection'>PRATOS<h1>
            <h2 class="lowFontDescript uppercaseTitle">COMIDA <?php single_term_title(); ?><h2>
                <ul id="listaFoodSearch">
                    <li class = lowFontDescript>Buscar por nome:</li>
                    <li><input class="inputForm" id="inputNomeComida" type="text" name="NomeComida"></li>
                    <div id = "flexOrdenarEFiltros">
                    <div id ="flexOrdenar">
					<li class = lowFontDescript>Ordenar:</li>
                    <li class = lowFontDescript><?php 	do_action( 'woocommerce_before_shop_loop' ); ?></li>
                
                    </div>
                    
                    
                    <div id="filtroPreco">
                    <li class = lowFontDescript>Filtro de Preço:</li>
                    <div id="deAte">
                    <li class = lowFontDescript>De:</li>
                    <li><input class="inputForm" id="inputDe" type="text" name="De"></li>
                    <li class = lowFontDescript>Até:</li>
                    <li><input class="inputForm" id="inputAte" type="text" name="Até"></li>
                    </div>  
                    </div>
                    </div>
                </ul>
<?php

echo "<div class='testerapido'>";
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}
echo "</div>";  
	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */



?>
</section>
<?php
get_footer( 'shop' );
?>
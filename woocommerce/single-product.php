<?php get_header(); ?>

<section>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php 
    $product = wc_get_product(get_the_ID());
    $nome = $product->get_name();
    $imagem = $product->get_image();
    $preco = $product->get_price_html();
    $descricao = $product->get_short_description();
    $categoria = $product->get_categories();
    $id = get_the_ID();

    echo '<section id="marginsingleproduct">';
    echo '<div id="flexSingProd"><div><img>';
    echo    $imagem;
    echo '</img></div>';
    echo '<div id="textosSingProd"><h1 class= tituloSection>';
    echo    $nome;
    echo '</h1>';
    echo '<p class="lowFontDescript" id="descricaoSingle">';
    echo    $descricao;
    echo '</p>';
    /* adicionar contador e porção aqui*/
    echo '<div id = "flexContadorEPorcao"><h1> GDAGAGAG </h1> <h1> GDSAGOIAJG </h1></div>';
    echo '<div id="flexPrecoAdicionar"><p id="precoProduto">';
    echo $preco;
    echo '</p>';
    echo "<button id='botaoAdicionar' onclick='window.location= `?add-to-cart={$id}`'>ADICIONAR</button></div></div></div>";
    echo '<p id="maisComidaJaponesa">MAIS COMIDA'.strtoupper($categoria).'</p>';
    echo '</section>';
?>

<?php endwhile; else: ?>
<p>N/A Página</p>
<?php endif; ?>
    
</section>


<?php get_footer(); ?>